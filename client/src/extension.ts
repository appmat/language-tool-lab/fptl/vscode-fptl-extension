import * as path from 'path';
import { workspace, ExtensionContext, commands, window, QuickPickOptions, Uri } from 'vscode';
import { Executable, LanguageClient, LanguageClientOptions, } from 'vscode-languageclient';
import { FptlTerminal } from './commands/FptlTerminal';
import { Download } from './commands/download';



let client: LanguageClient;

export function activate(context: ExtensionContext) {
	new Download(context)
	new FptlTerminal(context)

	let debugOptions = { execArgv: ['--nolazy', '--inspect=6009'] };
		// 	"-agentlib:jdwp=transport=dt_socket,server=y,suspend=n,quiet=y,address=*:5005",
	let args = [
		"-jar",
		path.resolve(
			context.extensionPath,
			"server",
			"server.jar"
		),

	];
	let executable: Executable = {
		command: "java",
		args: args,
	};
	console.log(args)
	console.log(executable)

	let clientOptions: LanguageClientOptions = {
		documentSelector: [{ scheme: 'file', language: 'fptl' }],
		synchronize: {
			fileEvents: workspace.createFileSystemWatcher('**/.clientrc')
		}
	};

	// Create the language client and start the client.
	client = new LanguageClient(
		'languageServer',
		'Language Server',
		executable,
		clientOptions
	);
	console.log(client);
	client.start();
	console.log(client);
}

export function deactivate(): Thenable<void> | undefined {
	if (!client) {
		return undefined;
	}
	return client.stop();
}
