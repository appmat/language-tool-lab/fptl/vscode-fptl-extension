import { getApi, FileDownloader } from "@microsoft/vscode-file-downloader-api";
import { workspace, ExtensionContext, commands, window, Uri } from 'vscode';
import { copyFile, existsSync, mkdirSync } from 'fs';
import { Octokit } from '@octokit/core';


export class UploadFile {
    constructor(public name: string, public url: string) {
    }
}

export class Release {
    constructor(public name: string, public url: string, public files: Array<UploadFile>) { }
}

export class Download {
    constructor(private context: ExtensionContext) {
        let downloadCommand = commands.registerCommand('fptl.downloadFptl', (reason: any) => { this.downloadFptl() })
        this.context.subscriptions.push(downloadCommand)
    }

    private async getReleasesList(): Promise<Array<Release>> {
        let octokit = new Octokit()

        let array: Array<Release> = new Array()
        await octokit.request('GET /repos/{owner}/{repo}/releases', {
            owner: "Zumisha",
            repo: "FPTL"
        }).then(response => {
            const length = response.data.length

            response.data.forEach(tag => {
                let files = new Array<UploadFile>()
                tag.assets.forEach(file => {
                    files.push(new UploadFile(file.name, file.browser_download_url))
                })

                array.push(new Release(tag.tag_name, tag.html_url, files))
            });
        }, error => {
            console.log(error)
        })
        return array
    }

    private async downloadFptl() {
        try {
            let array: Array<Release>
            await this.getReleasesList().then(result => {
                array = result
            }, error => {
                window.showErrorMessage("Failed")
            })
            let names: string[] = new Array(array.length)
            for (let i = 0; i < array.length; ++i) {
                names[i] = array[i].name
            }

            window.showQuickPick(names).then((selection) => {
                if (!selection) {
                    return
                }
                array.forEach(element => {
                    if (selection == element.name) {
                        this.downloadFiles(element).then()
                    }
                });
            })
        } catch (err) {
            console.log(err)
            window.showErrorMessage(err)
        }
    }

    private async downloadFiles(release: Release) {
        // Здесь есть возможность выбирать файлы в зависимости от ОС/конфигурации
        let arr = new Array<Promise<void>>()
        release.files.forEach(file => {
            arr.push(this.downloadAndMove(file).then())
        })
        Promise.all(arr).then(results => {
            window.showInformationMessage(`Download complete ${release.name} \nInformation ${release.url}`)
        })
    }

    private async downloadAndMove(uploadFile: UploadFile) {
        try {
            const fileDownloader: FileDownloader = await getApi();
            if (workspace.workspaceFolders == null) {
                window.showInformationMessage("Error, not found folder!")
                return
            }
            let folder = workspace.workspaceFolders[0].uri.fsPath + "\\.fptl"


            const file: Uri = await fileDownloader.downloadFile(
                Uri.parse(uploadFile.url),
                uploadFile.name,
                this.context
            );

            if (!existsSync(folder)) {
                mkdirSync(folder)
            }

            copyFile(file.fsPath, folder + `\\${uploadFile.name}`, err => {
                window.showErrorMessage(err.message)
            })
        } catch (err) {
            console.log(err)
            window.showErrorMessage(err)
        }
    }
}