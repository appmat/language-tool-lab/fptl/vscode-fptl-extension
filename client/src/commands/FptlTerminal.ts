import { existsSync } from 'fs';
import { window, ExtensionContext, commands, Terminal, workspace } from 'vscode';


export class FptlTerminal {
    constructor(private context: ExtensionContext) {
        this.registerCommands()
    }
    private terminal: Terminal = null

    private registerCommands() {

        let runInTerminal = commands.registerCommand('fptl.execInTerminal', async () => {
            await this.executeFileInTerminal(window.activeTextEditor.document.uri.fsPath).catch((ex) =>
                console.log('Failed to execute file in terminal', ex),
            );
        })

        this.context.subscriptions.push(runInTerminal)
    }
    private async executeFileInTerminal(fsPath: string) {
        // Todo("Configuration path to fptl")
        if (!existsSync(workspace.workspaceFolders[0].uri.fsPath + "\\.fptl\\fptl.exe")) {
            window.showErrorMessage("Not found \\.fptl\\fptl.exe")
            return
        }
        
        if (this.terminal == null) {
            this.terminal = window.createTerminal("Fptl")
            this.context.subscriptions.push(
                window.onDidCloseTerminal(t => {
                    if (t == this.terminal) {
                        this.terminal = null
                    }
                })
            )
        }
        this.terminal.sendText(`.fptl\\fptl.exe "${fsPath}"`)
        this.terminal.show(true)
    }
}