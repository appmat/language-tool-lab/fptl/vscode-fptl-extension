# FPTL language server for VSCode

***
[EN]

### Language Server with *LSP* support for *FPTL*

Implemented: syntax highlighting, download fptl, run programs, description of standard functions and word suggestion by prefix. Works with files with
the **.fptl** extension.

![example](https://gitlab.com/appmat/language-tool-lab/fptl/vscode-fptl-extension/-/raw/master/img/screenshot_1.png)

*Requires the JRE to work not less than 8.*

commands(ctrl+shift+p):
* fptl.downloadFptl
* fptl.execInTerminal

### About the language

FPTL (Functional Programming Typified Language) is a language in which the programmer does not need to explicitly work
with the mechanisms for organizing parallel computing. [More info about language.](https://github.com/Zumisha/FPTL/wiki)
___
[RU]

### Языковой сервер с поддержкой *LSP* для языка *FPTL*

Реализованы: подсветка синтаксиса, скачивание fptl, запуск программ, описание стандартных функций и предложение слова по префиксу. Работает с файлами с
расширением **.fptl**.

![example](https://gitlab.com/appmat/language-tool-lab/fptl/vscode-fptl-extension/-/raw/master/img/screenshot_1.png)

*Для работы требуется JRE не ниже 8.*

### О языке

FPTL (Functional Programming Typified Language) - это язык, в котором программисту не нужно явно работать с
инструментами для организации параллельных вычислений.
[Подробнее о языке.](https://github.com/Zumisha/FPTL/wiki)
